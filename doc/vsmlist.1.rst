=======
vsmlist
=======

----------------------------
the very simple mailing list
----------------------------

:Author: Tobias Leupold <tl at stonemx dot de>
:Homepage: https://nasauber.de/opensource/vsmlist/
:Date: @LASTCHANGE@
:Manual section: 1

Description
===========

``vsmlist`` is the \ *v*\ ery \ *s*\ imple \ *m*\ ailing list. It has been written to manage small mailing lists for everybody who wants to "just" setup a mailing list for e. g. a club without having to mess around with e. g. `Mailman <http://list.org/>`_, `Majordomo <http://greatcircle.com/majordomo/>`_ or `Sympa <http://sympa.org/>`_.

The list management is done in simple and editable text files. ``vsmlist`` won't scale very good with a large number of subscribers, so if you want to manage thousands of email addresses, have a look at it's big brothers.

``vsmlist`` is written in `Python <http://python.org/>`_ 3 and just uses the standard library without further dependencies (with `bind-tools' <https://www.isc.org/bind/>`_ ``dig`` being used to check DMARC policies if found, so the bind-tools are a soft dependency). That means you just have to have Python 3 itself installed to be able to use it (although having the bind-tools installed is a good idea). It's just one single file. No compiling has to be done, but you have to do some setup on your mail server.

Using vsmlist
=============

When all configuration is done, ``vsmlist`` can be used by simply sending emails to the mailing list's address.

For example, you could send an (empty) email to *mailinglist+subscribe@server* to subscribe the mailing list *mailinglist@server*. ``vsmlist`` will add the sender to the respective mailing list's config file. After that, you can simply send an email to *mailinglist@server* and it will be distributed to all subscribers of that list. By default, ``vsmlist`` will also archive the email in a local maildir.

The mailing list can be addressed via the normal *To* header or the *Cc* header. Addressing it via the *Bcc* header is not possible, as this one will be removed by the mail server and ``vsmlist`` will not be able to see which mailing list it should serve. Additional recipients can also be addressed. They will simply be ignored by ``vsmlist``.

Very simple, isn't it? :-)

Installation
============

``vsmlist`` consists only of one file. Just put the ``vsmlist`` executable somewhere on your system, e. g. into ``/usr/bin/``.

Invocation
==========

``vsmlist`` takes no command line options. It reads an email from ``STDIN`` and silently processes the data. Errors are written to ``STDERR``. All actions are logged using the system logger.

Configuring your mail server
============================

You have to tell your mail server to deliver emails sent to a mailing list's address to the ``vsmlist`` executable (saying piping the email to it) and not to a local mailbox or maildir.

Please read the documentation of your mail server on how to do that. When you use `Postfix <http://postfix.org/>`_, you could have a look to ``config_postfix.txt``, where a sample configuration is shown.

When server does what it should, you just have to create a per-mailing-list config file for your mailing list (see below) and ``vsmlist`` is ready to go.

SPF and DKIM
------------

Be sure to have `SPF <https://en.wikipedia.org/wiki/Sender_Policy_Framework>`_ and `DKIM <https://en.wikipedia.org/wiki/DomainKeys_Identified_Mail>`_ setup correctly. If so, mails sent through ``vsmlist`` will pass both, as their content is not altered (only a few headers are added).

DMARC
-----

`DMARC <https://en.wikipedia.org/wiki/DMARC>`_ is a disputed standard intended to reduce spam. It demands so-called "alignment", which means that the addresses in the ``Return-Path``, ``From`` and ``DKIM-Signature`` headers have to use the same domain. DMARC breaks `RFC 5322 <https://tools.ietf.org/html/rfc5322>`_, because it ignores the ``Sender`` header (it should check) and only looks at the ``From`` header instead. This causes a lot of trouble with the way mailing have always been working. Also, there's no real solution addressing this problem, but only workarounds with drawbacks and tradeoffs.

The approach to elude deliverability problems caused by DMARC policies chosen for ``vmslist`` is: If a sender's domain sets the ``reject`` or ``quarantine`` DMARC policy, the mail is not simply forwarded, but wrapped inside a new email that is sent by the mailing list itself, along with a notice why this was necessary. This will (provided the mail server is setup properly) lead to an SPF, DKIM and DMARC compliant, totally aligned mail (with the drawback that the original mail is not accessible directly).

To be able to check a sender's domain's DMARC policy, ``vsmlist`` uses bind-tool's ``dig`` command. If it's not found, it handles all sender domains as if they had no DMARC policy set.

Configuring vsmlist
===================

All config files are .ini style files with sections and options. A section is enclosed by square brackets. An option is followed by a "=". Everything that follows is the options's value, without quoting. A section or option is always ended by a newline. To use newlines in an option's value, use *\\n*.

``vsmlist`` processes configuration data in three steps. First, the default values are set, then, the system-wide config file is parsed, and finally, a per-mailing-list config file is parsed.

Be sure that ``/var/lib/vsmlist/`` can be read and written by the user that will execute ``vsmlist``.

System-wide configuration
-------------------------

The system-wide configuration file is ``/etc/vsmlist.ini``. When it's not found or some options are not set, the default values are used.

The following sections and options can be set:

**[server]**

    **host**
        The hostname of the mailserver. This is smtplib.SMTP's *host* parameter. Defaults to *localhost*.

    **port**
        The port of the mailserver. This is smtplib.SMTP's *port* parameter. Defaults to *25*.

    **delimiter**
        A character used to separate a command from the mailing list's name. Default is *+*, so a command for the mailing list *list@server* would be *list+command@server*.

        Be sure to configure your mail server to process the delimiter, so that an email for *list+command@server* is delivered to *list@server*.

**[vsmlist]**

    **dataDir**
        Location of the mailing list config files and the mailing list maildirs. Defaults to */var/lib/vsmlist/*.

    **processedIdDir**
        Directory for temporary files used for storing the Message-IDs of all mails processed. Defaults to */var/lib/vsmlist/processed/*.

    **daemon**
        The mailing list's address for system generated mails. Default is *vsmlist-daemon*. The server will be the server's name of the mailing list.

    **encodings**
        Space-separated list of encodings to try when generating emails. ``vsmlist`` will try to encode a generated email in this order until it can be encoded with the respective codec. Default is *ascii iso-8859-1 iso-8859-15 utf-8*.

        The last one, *utf-8* should always work and should be included in this list as a fall-back encoding.

    **archive**
        Bool value to turn on and off archiveing. Default is *yes*. So, by default, each mail sent over the mailing list is added to a maildir below what **dataDir** is set to (default: ``/var/lib/vsmlist/``) named like the mailing lists's email address (like ``/var/lib/vsmlist/mailing_list@server/``). Can be *yes*, *no*, *true*, *false*, *0* and *1*.

        ``vsmlist`` just archives the mails. Nothing is done with this data, this is your job ;-)

    **notifySubscription**
        Bool value that indicates whether or not the mailing list's admin should be informed if a new member is added to a mailing list.

    **notifyUnsubscription**
        Bool value that indicates whether or not the mailing list's admin should be informed if a member is removed from a mailing list.

    **answerNotSubscribed**
        Bool value that indicates whether or not a mail to a mailing list the sender is not subscribed to should be answered with an error message explaining how to subscribe or silently be ignored.

**[commands]**

    **subscribe**
        Command to subscribe to the list. Default is *subscribe*.

    **unsubscribe**
        Command to unsubscribe to the list. Default is *unsubscribe*.

    **admin**
        Command to send an email to the list's administrator. Default is *admin*.

    **help**
        Command to request a list of all available commands (also included in the "Welcome" email). Default is *help*.

    **bounces**
        The email address with this command is used in the ``Return-Path`` and ``Errors-To`` headers (and thus not intended to be used by a list user). Mail sent to this address is forwarded to the list admin. Default is *bounces*.

**[subjects] and [texts]**

    These two sections share the same option names. They contain subject and text templates for system generated emails. Strings enclosed in curly braces are replaced by the respective variables, using Python's str.format() function. See ``vsmlist.ini.default`` for the default settings.

    The strings to be replaced used in the default config are available, per setting. Setting other references will crash vsmlist (please test your settings!). The references can be used multiple times.

    The following mails are generated by ``vsmlist``:

    **welcome**

        When a new user subscribes to the mailing list. Includes the "help" email.

    **help**

        When a user requests help / a list of commands.

    **goodbye**

        When a user unsubscribes from the mailing list.

    **alreadySubscribed**

        When a user tries to subscribe to a mailing list where he is already subscribed.

    **notSubscribed**

        When a user sends an email to a mailing list but isn't subscribed to it.

    **commandNotDefined**

        When a user queries a mailing list with an undefined command.

    **subscriptionReport**

        Notification to the mailing list's admin sent if a new member is added.

    **unsubscriptionReport**

        Notification to the mailing list's admin sent if a member is removed.

    **dmarcMessage**

        This text is added to an email that had to be wrapped into a new one due to a restrictive DMARC policy of the original sender's domain to avoid deliverability issues. There's no **subject** for this text.


Per-mailing-list configuration
------------------------------

The configuration per mailing list is done in .ini files in what **dataDir** is set to (default: ``/var/lib/vsmlist/``). These files have to be named with the mailing lists's email address, like ``/var/lib/vsmlist/mailing_list@server.ini``. All options of the system-wide configuration file can be set in such a file. If set, their values will replace the default ones or those set in ``/etc/vsmlist.ini``.

Additionally, there are two sections:

**[setup]**

    Contains just one key: **admin**. This has to be set to the email address of the list's admin. This is the only thing you have to do to setup a mailing list (of course after configuring your mail server!): create it's per-list config file, add the *[setup]* section and the *admin* option. Such a file would be:

        ::

            [setup]
            admin = admin_email@server

    That's all :-)

    The admin's email address is not automatically included in the subscriber's list. Don't forget to subscribe the admin's email address to the mailing list if this is desired.

**[subscribers]**

    This section holds a list of all subscribers of the mailing list in the form *subscriber's_address@server = date*, where date is formatted as *YYYY-MM-DD*. When a user requests subscription to a mailing list, ``vsmlist`` adds it's address and the current date to the list's config file.

    This can of course also be done manually by simply editing this file. The date isn't used by ``vsmlist``, so you can put whatever you want there.
