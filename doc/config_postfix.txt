Configuring Postfix
===================

piping an email to a program instead of storing it to a mailbox can be done
quite easy. This example supposes that vsmlist has been installed to
"/usr/bin/vsmlist", a user named "vsmlist" has been created to execute the
program and transport tables are stored in "/etc/postfix/transport".

Add the following lines to the respective files:

/etc/postfix/master.cf
        vsmlist   unix  -       n       n       -       10      pipe
          flags=DRhu user=vsmlist argv=/usr/bin/vsmlist

/etc/postfix/transport
        mailing_list@server vsmlist:

/etc/postfix/main.cf
        transport_maps = texthash:/etc/postfix/transport

Don't forget to restart Postfix. If you already use something like
hash:/etc/postfix/transport, of course, you have to run postmap on the file
after changing it.
