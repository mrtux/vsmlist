#!/usr/bin/env python3

# SPDX-FileCopyrightText: 2012-2022 Tobias Leupold <tl at stonemx dot de>
#
# SPDX-License-Identifier: GPL-3.0-or-later

""" vsmlist - the very simple mailing list """

import sys
import os
import time

if sys.version_info < (3, 2):
    from configparser import SafeConfigParser as ConfigParser
else:
    from configparser import ConfigParser

import codecs
import re
import syslog

from datetime import datetime

from email import message_from_string

from email.header import Header

from email.utils import parseaddr
from email.utils import getaddresses

from email.mime.text import MIMEText
from email.mime.message import MIMEMessage
from email.mime.multipart import MIMEMultipart

from smtplib import SMTP

from mailbox import Maildir

from hashlib import md5

from subprocess import check_output

def updateConfig(iniFile):
    """Read an ini file and update the current config settings"""

    c = ConfigParser()
    try:
        c.read_file(codecs.open(iniFile, 'r', 'utf8'))
    except:
        # The file does not exist or is not readable, so we don't need to parse it
        return

    # Walk through all sections of the default config
    for section in defaultConfig:
        for option in defaultConfig[section]:
            try:
                config[section][option] = c.get(section, option).replace('\\n', '\n')
            except:
                pass

def abort(message):
    """Send a message to syslog and stdout and quit"""

    # Log the reason for the abort
    syslog.syslog(message)

    # Also print it out, as this will be included in the
    # fail notice mail that will be sent to the requesting user
    # A user shouldn't see this anyway ...
    print(message)

    # Break here
    sys.exit(1)

def getEncoding(string):
    """Get a suitable encoding for a string"""
    for encoding in encodings:
        try:
            string.encode(encoding)
            return encoding
        except:
            pass

def encodeHeader(header):
    """Encode an email header with a suitable encoding"""
    return Header(header, getEncoding(header))

def sendEmail(From, To, Subject, payload):
    """Send a plain-text email"""

    # Create a new email using a suitable encoding
    msg = MIMEText(payload, _charset = getEncoding(payload))

    # Append the headers choosing the right encoding
    msg['From'] = encodeHeader(From)
    msg['To'] = encodeHeader(To)
    msg['Subject'] = encodeHeader(Subject)

    # Send the email
    mailer.sendmail(From, To, msg.as_string())

def assembleMultipartMail(From, To, Subject, text, attachedMail):
    """Create a mail with a text and an attached inline-viewed mail"""

    message = MIMEMultipart()

    # Append the headers choosing the right encoding
    message['From'] = encodeHeader(From)
    message['To'] = encodeHeader(To)
    message['Subject'] = encodeHeader(Subject)

    # Add a MIME multipart hint
    message.preamble = 'This is a multi-part message in MIME format.'

    # Attach the text part
    message.attach(MIMEText(text, _charset = getEncoding(text)))

    # Attach the mail to be viewed
    message.attach(MIMEMessage(attachedMail))

    return message

def sendErrorEmail(From, To, Subject, text):
    """Send an email including the original query email as an attachment"""
    mailer.sendmail(From, To,
                    assembleMultipartMail(From, To, Subject, text, query['mail']).as_string())

def writeConfig():
    """Save the configuration -- needed for (un)subscribing"""
    try:
        listConfig.write(codecs.open(listConfigFile, 'w', 'utf8'))
    except:
        abort('Could not write to config file "' + listConfigFile + '". Aborting.')

def makeBool(value):
    """Translate an ini file string value to a boolean value"""
    value = value.lower()
    if value == 'yes' or value == 'on' or value == '1':
        return True
    elif value == 'no' or value == 'off' or value == '0':
        return False
    else:
        return None

def getAddressComponents(address):
    """Get the list, command and server information of an email address"""
    delimiter = re.escape(config['server']['delimiter'])
    tmp = re.search('([^' + delimiter + ']+)' + delimiter + '?(.+)?@(.+)', address)
    return(tmp.group(1), tmp.group(2), tmp.group(3))

def genHash():
    """Calculate a hash from a part of the message"""
    return(md5(rawMailData[0:100].encode('utf8')).hexdigest())

def getMessageId():
    """Get or generate an unique message ID"""

    # Try to get the mail's Message-ID header
    id = query['mail'].get('Message-ID')
    if id == None:
        # We have no ID. So we generate one and return it.
        return(genHash() + '.' + query['mail'].get('From'))

    # Remove the tags from the ID.
    id = id[1:-1]
    if '/' in id:
        # We have a '/' in the ID. This will cause problems, so remove it.
        # Add a hash just to be sure not to generate a collision (hopefully)
        id = id.replace('/', '') + '.' + genHash()

    return(id)

def storeIdProcessed():
    """Create an ID file marking a message as processed"""

    # Check if the ID files directory exists and create it if not
    if not os.path.exists(config['vsmlist']['processedIdDir']):
        os.makedirs(config['vsmlist']['processedIdDir'])

    # Touch the ID file
    open(config['vsmlist']['processedIdDir'] + query['messageId'], 'a').close()

    # Wipe ID files older than one day
    for idFile in os.listdir(config['vsmlist']['processedIdDir']):
        fileName = config['vsmlist']['processedIdDir'] + idFile
        if time.time() - os.path.getmtime(fileName) > 86400:
            os.unlink(fileName)

def checkIdProcessed():
    """Check if a message has already been processed by vsmlist"""
    return os.path.exists(config['vsmlist']['processedIdDir'] + query['messageId'])

def getDmarcPolicy(domain):
    """Lookup a domain's DMARC policy using dig and parse it"""

    # First we assume no DMARC policy is set
    entry = ''
    p = 'not set'
    sp = 'not set'

    # Try to lookup the policy using dig. If we don't have dig, this will fail. In this case,
    # we can't handle DMARC and treat it as if no DMARC policy was defined.
    try:
        entry = check_output(['dig', '+short', '_dmarc.' + domain, 'TXT']).decode().strip()
    except:
        return p, sp

    # Parse the entry

    entry = entry[1:-1]
    parts = entry.split(';')
    parsed = {}

    for part in parts:
        part = part.strip()
        index = part.find('=')

        if index == -1:
            continue

        parsed[part[0:index].lower()] = part[index + 1:].lower()

    # Check for the correct version
    if not 'v' in parsed or parsed['v'].lower() != 'dmarc1':
        return p, sp

    # Get the domain and subdomain policy (if set)
    if 'p' in parsed:
        p = parsed['p']
    if 'sp' in parsed:
        sp = parsed['sp']

    return p, sp

############################
### HERE STARTS THE CODE ###
############################

# Output the version and exit
if len(sys.argv) > 1:
    if len(sys.argv) == 2 and sys.argv[1] == '-v':
        print('@VERSION@')
        sys.exit(0)
    else:
        print('Invalid command line argument. There\'s only one command line argument:')
        print('    {} -v: Display the program version and exit.'.format(sys.argv[0]))
        sys.exit(0)

# Define the tag used for the syslog, log to mail.log
syslog.openlog('vsmlist', 0, syslog.LOG_MAIL)

# This is the default configuration.
# It will be first overwritten by the values in /etc/vsmlist.ini
# and then by the values from the respective mailing list ini file.

# CAUTION: Leave the following comment here!
# @BEGIN_DEFAULT_CONFIG@
defaultConfig = {
    'server': {
        'host':      'localhost',
        'port':      '25',
        'delimiter': '+'
    },
    'vsmlist': {
        'dataDir':              '/var/lib/vsmlist/',
        'processedIdDir':       '/var/lib/vsmlist/processed/',
        'daemon':               'vsmlist-daemon',
        'encodings':            'ascii iso-8859-1 iso-8859-15 utf-8',
        'archive':              'yes',
        'notifySubscription':   'yes',
        'notifyUnsubscription': 'yes',
        'answerNotSubscribed':  'yes'
    },
    'commands': {
        'subscribe':   'subscribe',
        'unsubscribe': 'unsubscribe',
        'admin':       'admin',
        'help':        'help',
        'bounces':     'bounces'
    },
    'subjects': {
        'welcome':              'Subscription request to mailing list "{list}"',
        'goodbye':              'Unsubscription request to mailing list "{list}"',
        'alreadySubscribed':    'Subscription request to mailing list "{list}"',
        'notSubscribed':        'Query to mailing list "{list}"',
        'commandNotDefined':    'Query to mailing list "{list}"',
        'subscriptionReport':   'Subscription request to mailing list "{list}"',
        'unsubscriptionReport': 'Unsubscription request to mailing list "{list}"',
        'help':                 'Help request for mailing list "{list}"'
    },
    'texts': {
        'help': (
            'The mailing list can be used as follows:\n\n'
            'If you send an email to "{list}", it will be distributed to all subscribers.\n\n'
            'Additionally, the are some "command" email addresses that can be used by simply '
            'sending an (empty) email to them. The following command email addresses are '
            'available:\n\n'
            '{help}\n'
            '    Request this email\n\n'
            '{subscribe}\n'
            '    Subscribe the sender\'s email address to the list\n\n'
            '{admin}\n'
            '    Forward the sent email to the mailing list\'s administrator\n\n'
            '{unsubscribe}\n'
            '    Unsubscribe the sender email address from the list'
        ),
        'welcome': (
            'Welcome to the mailing list "{list}"!\n\n'
            '{helpText}\n\n'
            'Have a lot of fun :-)'
        ),
        'goodbye': (
            'Your email address has been unsubscribed from the mailing list "{list}". If you want '
            'to be subscribed again, send an email to "{subscribe}".'
        ),
        'alreadySubscribed': (
            'You are already a member of the mailing list "{list}".\n\n'
            'The email you sent follows as an attachment.'
        ),
        'notSubscribed': (
            'You are not subscribed to the mailing list "{list}". To subscribe, send an email to '
            '"{subscribe}".\n\n'
            'The email you sent follows as an attachment.'
        ),
        'commandNotDefined': (
            'The command "{command}" is not defined. To get a list of all available commands, '
            'send an email to "{help}".\n\n'
            'The email you sent follows as an attachment.'
        ),
        'subscriptionReport': (
            'The email address "{address}" has just been added to the mailing list "{list}".'
        ),
        'unsubscriptionReport': (
            'The email address "{address}" has just been removed from the mailing list "{list}".'
        ),
        'dmarcMessage': (
            'The DMARC policy of the original sender\'s domain obstructs mailing lists to\n'
            'function in the traditional way (simply forwarding the mail to all\n'
            'subscribers). We thus are forced to send the original mail as an attachment\n'
            'to migitate problems with delivery.\n\n'
            'The original message follows:'
        )
    }
}
# CAUTION: Leave the following comment here!
# @END_DEFAULT_CONFIG@

# First, we set all values to the default ones
config = defaultConfig

# Check for /etc/vsmlist.ini and overwrite the default values with the global ones
updateConfig('/etc/vsmlist.ini')

# The encodings to try
encodings = re.split('\s+', config['vsmlist']['encodings'])

# Process the piped mail

# Set stdin reading to binary
sys.stdin = sys.stdin.detach()

# Get the raw mail as binary data
rawMailData = sys.stdin.read()

query = {}

# Convert the binary data to a string
for encoding in encodings:
    try:
        rawMailData = rawMailData.decode(encoding)
        query['encoding'] = encoding
        break
    except:
        pass

# Parse the email
query['mail'] = message_from_string(rawMailData)

# Get the message id or generate one
query['messageId'] = getMessageId()

# Check if this message has already been processed
# (happens when multiple mailing lists are addressed in one email)
if checkIdProcessed() == True:
    syslog.syslog(('Message {} has already been processed or is processing right now. '
         'Skipping it.').format(query['messageId']))
    sys.exit(0)

# Mark the message as processed
storeIdProcessed()

# Parse the From header

query['originalFrom'] = query['mail'].get('From')
parsedFrom = parseaddr(query['originalFrom'])

if not '@' in parsedFrom[1] or not '.' in parsedFrom[1]:
    syslog.syslog('No valid mail address given in the From header. '
                  'Can\'t process Message {}.'.format(query['messageId']))
    sys.exit(0)

query['from'] = parsedFrom[1].lower()
query['fromDisplay'] = parsedFrom[0]
if parsedFrom[0] == '':
    query['fromDisplay'] = query['from']

query['domain'] = query['from'].split('@')[1]
domainParts = query['domain'].split('.')
query['topLevelDomain'] = domainParts[-2] + '.' + domainParts[-1]

# Get all recipients from the To and Cc header

rawRecipients = []
for header in ['To', 'Cc']:
    if query['mail'].get(header):
        rawRecipients.append(query['mail'].get(header))

recipients = []
for recipient in getaddresses(rawRecipients):
    recipients.append(recipient[1].lower())

# Check all addresses for being a mailing list

checkedRecipients = []
replyTo = ''

for address in recipients:
    try:
        # Parse the address
        (listPart, commandPart, serverPart) = getAddressComponents(address)

        # Try to open the respective config file
        codecs.open('{}{}@{}.ini'.format(config['vsmlist']['dataDir'], listPart, serverPart),
                    'r', 'utf8')

        # If we reached here, we host the list with the checked address, so ...

        # ... add the address to the address list we process later ...
        checkedRecipients.append(address)

        # ... and update the Reply-To-Header we will set

        # We only update the Reply-To header if the email will be actually distributed.
        # It won't be if we have a "command", so:
        if commandPart == None:
            if replyTo == '':
                replyTo = listPart + '@' + serverPart
            else:
                replyTo = replyTo + ', ' + listPart + '@' + serverPart

    except:
        # The "open" statement above failed, so we don't host this address
        pass

# Check if we actually found a mailing list we host.
# The mail server shouldn't send an email that doesn't contain at least one mailing list to this
# script ... but who knows?! ;-)
if len(checkedRecipients) == 0:
    abort(('None of the following email addresses is a mailing list hosted here:\n\n'
           '{}\n\n'
           'Aborting.').format('\n'.join(recipients)))

# Process all mailing lists that have been addressed

# Setup the mailer
mailer = SMTP(host = config['server']['host'], port = int(config['server']['port']))

for listToProcess in checkedRecipients:
    # Parse the address we want to process
    (query['list'], query['command'], query['server']) = getAddressComponents(listToProcess)

    # Let's setup the list config ...
    listConfig = ConfigParser()
    listConfigFile = config['vsmlist']['dataDir'] + query['list'] + '@' + query['server'] + '.ini'
    try:
        listConfig.read_file(codecs.open(listConfigFile, 'r', 'utf8'))
    except:
        abort('Could not process mailing list "{}@{}". Aborting.'.format(query['list'],
                                                                         query['server']))

    # ... check for a proper setup ...
    config['setup'] = {}
    try:
        config['setup']['admin'] = listConfig.get('setup', 'admin')
    except:
        abort('The mailing list {}@{} has not been setup yet. Aborting.'.format(query['list'],
                                                                                query['server']))

    # ... and update the config with the list-specific config file
    updateConfig(listConfigFile)

    # Translate the raw command to the actual command string
    if query['command'] != None:
        for command in defaultConfig['commands']:
            if query['command'] == config['commands'][command]:
                query['command'] = command
                break
    else:
        query['command'] = 'distribute'

    # Assemble some email addresses
    config['addresses'] = {
        'daemon':
            config['vsmlist']['daemon'] + '@' + query['server'],
        'list':
            query['list'] + '@' + query['server'],
        'subscribe':
            query['list'] + config['server']['delimiter'] + config['commands']['subscribe']
            + '@' + query['server'],
        'unsubscribe':
            query['list'] + config['server']['delimiter'] + config['commands']['unsubscribe']
            + '@' + query['server'],
        'admin':
            query['list'] + config['server']['delimiter'] + config['commands']['admin']
            + '@' + query['server'],
        'help':
            query['list'] + config['server']['delimiter'] + config['commands']['help']
            + '@' + query['server'],
        'bounces':
            query['list'] + config['server']['delimiter'] + config['commands']['bounces']
            + '@' + query['server']
    }

    # Start the processing

    # Log what we are doing
    if query['command'] == 'subscribe':
        syslog.syslog('Received request to add <{0}> to mailing list <{1}>'.format(
            query['from'], config['addresses']['list']))
    elif query['command'] == 'unsubscribe':
        syslog.syslog('Received request to remove <{0}> from mailing list <{1}>'.format(
            query['from'], config['addresses']['list']))
    elif query['command'] == 'admin':
        syslog.syslog('Received email for admin of mailing list <{0}> from <{1}>'.format(
            config['addresses']['list'], query['from']))
    elif query['command'] == 'help':
        syslog.syslog('Received a help request for mailing list <{0}> from <{1}>"'.format(
            config['addresses']['list'], query['from']))
    elif query['command'] == 'bounces':
        syslog.syslog('Received a bounce mail for mailing list <{0}>" from <{1}>"'.format(
            config['addresses']['list'], query['from']))
    elif query['command'] == 'distribute':
        syslog.syslog('Received a mail for mailing list <{0}>" from <{1}>"'.format(
            config['addresses']['list'], query['from']))
    else:
        syslog.syslog(('Received request with undefined command "{0}" for mailing list <{1}> '
                       'from <{2}>').format(
                       query['command'], config['addresses']['list'], query['from']))

    # Check if the requester is a member of the mailing list

    # There are two cases in which the sender could not be subscribed yet:

    # We want to subscribe
    if query['command'] == 'subscribe':
        # Check if the requested address has been already subscribed
        if listConfig.has_option('subscribers', query['from']):
            syslog.syslog('<{0}> is already a member of the mailing list <{1}>'.format(
                query['from'], config['addresses']['list']))

            # Inform the requesting user
            sendErrorEmail(config['addresses']['daemon'],
                           query['from'],
                           config['subjects']['alreadySubscribed'].format(
                               list = config['addresses']['list']),
                           config['texts']['alreadySubscribed'].format(
                               list = config['addresses']['list']))

            syslog.syslog('Sent error message to <{}>'.format(query['from']))

            # Abort the script
            mailer.quit()
            sys.exit(0)

    # We received a bounced mail
    elif query['command'] == 'bounces':
        # Forward the email to the admin
        mailer.sendmail(query['originalFrom'],
                        config['setup']['admin'],
                        query['mail'].as_string())
        syslog.syslog('Forwarded bounced mail to <{}>'.format(config['setup']['admin']))

        # Abort the script
        mailer.quit()
        sys.exit(0)

    # In all other cases, the requester must be subscribed, so:
    else:
        if not listConfig.has_option('subscribers', query['from']):
            syslog.syslog('<{0}> is not a member of the mailing list <{1}>'.format(
                query['from'], config['addresses']['list']))

            if makeBool(config['vsmlist']['answerNotSubscribed']):
                # Inform the requesting user
                sendErrorEmail(config['addresses']['daemon'],
                               query['from'],
                               config['subjects']['notSubscribed'].format(
                                   list = config['addresses']['list']),
                               config['texts']['notSubscribed'].format(
                                   list = config['addresses']['list'],
                                   subscribe = config['addresses']['subscribe']))

                syslog.syslog('Sent error message to <{}>'.format(query['from']))

            else:
                syslog.syslog('Silently ignoring the mail sent to the mailing list <{0}> by '
                              '<{1}>'.format(config['addresses']['list'], query['from']))

            # Abort the script
            mailer.quit()
            sys.exit(0)

    # We want to subscribe the email sender
    if query['command'] == 'subscribe':
        # Check if we already have subscriptions
        if not listConfig.has_section('subscribers'):
            listConfig.add_section('subscribers')

        # Be sure not to subscribe the list to itself
        # Only bad, bad people would fake an email with the list address
        # as From and send a subscription request -- but who knows?! ;-)
        if query['from'] == config['addresses']['list']:
            # Inform the admin
            sendErrorEmail(config['addresses']['daemon'],
                           config['setup']['admin'],
                           'Malicious subscription request for mailing list "{}"'.format(
                               config['addresses']['list']),
                           ( 'Someone tried to subscribe the mailing list to itself. '
                             'Aborting to prevent an infinite loop. The request email follows.'))

            # Abort the script
            abort(('Won\'t subscribe the list to itself. '
                   'Sent report to the list admin "{}". '
                   'Aborting.').format(config['setup']['admin']))

        # Update the config
        listConfig.set('subscribers', query['from'], datetime.now().strftime('%Y-%m-%d'))
        writeConfig()
        syslog.syslog('Added <{0}> to the mailing list <{1}>'.format(
                      query['from'], config['addresses']['list'] + '"'))

        # Inform the requesting user
        sendEmail(config['addresses']['daemon'],
                  query['from'],
                  config['subjects']['welcome'].format(list = config['addresses']['list']),
                  config['texts']['welcome'].format(
                      list = config['addresses']['list'],
                      helpText = config['texts']['help'].format(
                          list = config['addresses']['list'],
                          help = config['addresses']['help'],
                          subscribe = config['addresses']['subscribe'],
                          admin = config['addresses']['admin'],
                          unsubscribe = config['addresses']['unsubscribe'])))

        if makeBool(config['vsmlist']['notifySubscription']):
            # Also inform the list's admin
            sendEmail(config['addresses']['daemon'],
                      config['setup']['admin'],
                      config['subjects']['subscriptionReport'].format(
                          list = config['addresses']['list']),
                      config['texts']['subscriptionReport'].format(
                          list = config['addresses']['list'],
                          address = query['from']))

    # We want to unsubscribe the email sender
    elif query['command'] == 'unsubscribe':
        # Update the config
        listConfig.remove_option('subscribers', query['from'])
        writeConfig()
        syslog.syslog('Removed <{0}> from mailing list <{1}>'.format(
                      query['from'], config['addresses']['list']))

        # Inform the requesting user
        sendEmail(config['addresses']['daemon'],
                  query['from'],
                  config['subjects']['goodbye'].format(list = config['addresses']['list']),
                  config['texts']['goodbye'].format(
                      list = config['addresses']['list'],
                      subscribe = config['addresses']['subscribe']))

        if makeBool(config['vsmlist']['notifyUnsubscription']):
            # Also inform the list's admin
            sendEmail(config['addresses']['daemon'],
                      config['setup']['admin'],
                      config['subjects']['unsubscriptionReport'].format(
                          list = config['addresses']['list']),
                      config['texts']['unsubscriptionReport'].format(
                          list = config['addresses']['list'],
                          address = query['from']))

    # We want to receive a help message
    elif query['command'] == 'help':
        sendEmail(config['addresses']['daemon'],
                  query['from'],
                  config['subjects']['help'].format(list = config['addresses']['list']),
                  config['texts']['help'].format(
                      list = config['addresses']['list'],
                      help = config['addresses']['help'],
                      subscribe = config['addresses']['subscribe'],
                      admin = config['addresses']['admin'],
                      unsubscribe = config['addresses']['unsubscribe']))

        syslog.syslog('Delivered help message to <{}>'.format(query['from']))

    # We want to send an email to the list admin
    elif query['command'] == 'admin':
        # Forward the email to the admin
        mailer.sendmail(query['originalFrom'],
                        config['setup']['admin'],
                        query['mail'].as_string())
        syslog.syslog('Forwarded mail to <{}>'.format(config['setup']['admin']))

    # We want to send an email to all users of the list
    elif query['command'] == 'distribute':
        # Archive the mail if requested
        if makeBool(config['vsmlist']['archive']) == True:
            # Setup the mailing list's mailbox
            try:
                mbox = Maildir(config['vsmlist']['dataDir'] + config['addresses']['list'],
                               create = True)
            except:
                abort('Could not create maildir "/var/lib/vsmlist/{}"'.format(
                    config['addresses']['list']))

            # Add the email to the mailing list's mailbox
            mbox.add(rawMailData.encode(query['encoding']))

        # Prepare the mail to be distributed

        distributedMail = ''

        errorsTo        = config['addresses']['bounces']
        listId          = config['addresses']['list'].replace('@', '.')
        listUnsubscribe = '<mailto:{}?subject={}>'.format(config['addresses']['unsubscribe'],
                                                          config['commands']['unsubscribe'])

        # Check if we can send the mail in the traditional way or if we have
        # to mess with the DMARC policy of the original sender's domain
        dmarcPolicy = 'not set'
        dmarcPolicyP, dmarcPolicySp = getDmarcPolicy(query['topLevelDomain'])
        if query['domain'] == query['topLevelDomain']:
            dmarcPolicy = dmarcPolicyP
        else:
            if dmarcPolicySp != 'not set':
                dmarcPolicy = dmarcPolicySp
            else:
                p, sp = getDmarcPolicy(query['domain'])
                if p != 'not set':
                    dmarcPolicy = p
                else:
                    dmarcPolicy = dmarcPolicyP

        if dmarcPolicy == 'not set' or dmarcPolicy == 'none':
            # No DMARC policy stops us from sending the mail the traditional way

            # We modify the original mail's headers

            distributedMail = query['mail']

            headers = {
                'Sender'          : '"{}" <{}>'.format(query['list'],
                                                       config['addresses']['bounces']),
                'Reply-To'        : replyTo,
                'Errors-To'       : errorsTo,
                'List-Id'         : listId,
                'List-Unsubscribe': listUnsubscribe
            }

            for header in headers:
                del distributedMail[header]
                distributedMail.add_header(header, headers[header])

        else:
            # The DMARC policy of the sender's domain prevents us from distributing the mail in the
            # traditional way. To prevent rejecting or quarantining, we create a new mail (that will
            # be aligned) and distribute the original mail as an attachment

            distributedMail = assembleMultipartMail(
                '{} via {} <{}>'.format(query['fromDisplay'], query['list'],
                                        config['addresses']['list']),
                config['addresses']['list'],
                query['mail']['Subject'],
                config['texts']['dmarcMessage'],
                query['mail'])

            distributedMail.add_header('Reply-To', replyTo)
            distributedMail.add_header('Errors-To', errorsTo)
            distributedMail.add_header('List-Id', listId)
            distributedMail.add_header('List-Unsubscribe', listUnsubscribe)

        # Distribute the mail to the list
        mailer.sendmail(config['addresses']['bounces'],
                        listConfig.options('subscribers'),
                        distributedMail.as_string())

        syslog.syslog('Distributed mail to <{}>'.format(config['addresses']['list']))

    # The command isn't defined
    else:
        sendErrorEmail(config['addresses']['daemon'],
                       query['from'],
                       config['subjects']['commandNotDefined'].format(
                           list = config['addresses']['list']),
                       config['texts']['commandNotDefined'].format(
                           command = query['command'],
                           help = config['addresses']['help']))
        syslog.syslog('Sent error message to <{}>'.format(query['from']))

# Close the smtp connection
mailer.quit()
