* Bugfix: Generate message id if not provided.

* Bugfix: Evaluate "vsmlist/notifySubscription" and "vsmlist/notifyUnsubscription" correctly
  (setting those to "no" had no effect)

* New: Added the "answerNotSubscribed" option to the "vsmlist" group. This defaults to "yes" and
  keeps the default behavior of answering mail from unsubscribed requesters with the info that the
  sender address is not subscribed to the queried mailing list.
  When set to "no", such requests are silently ignored (e.g. to prevent backscatter mail).

* Bugfix: Fixed processing of mails with a Reply-To header set.

* Change: Vsmlist now logs to the "Mail" facility (``syslog.LOG_MAIL``).

====================================================================================================
vsmlist 0.3 released (15.10.2021)
====================================================================================================

* Enhancement: Using bind-tools' ``dig``, vsmlist now looks up if a sender's domain sets a DMARC
  policy that could lead to delivery problems. If so, the sender's original mail is wrapped into a
  new one and sent from the mailing list itself, letting DMARC pass (provided the mail server does
  correct DKIM signing and SPF has been setup properly).

* Enhancement: Added bounces handling through setting the ``Return-Path`` and ``Errors-To`` headers
  to a list bounces address. Mail sent to this address is forwarded to the list admin. This also
  leads to the SPF setting being okay, as the ``Reply-To`` header matches the ``Sender``.

* Enhancement: Added ``Sender``, ``List-Id`` and ``List-Unsubscribe`` headers

* Bugfix: Fixed a configparser deprecation warning with Python >= 3.2.

* Change: The "subscribers" command has been removed.

* Change: Added a release script to do various string replacements, file generation and so on.

* Enhancement: Added a "help" command sending an email with all command email addresses.

* Enhancement: Added options to send an email to the list's admin if a member is added or removed to
  or from a mailing list.

* Enhancement: Added custom "host" and "port" paramters of smtplib.SMTP to the config.

====================================================================================================
vsmlist 0.2 released (05.01.2015)
====================================================================================================

* Changes:

  - Coding style rework and code cleanup.

  - Changed the string formatting from the old-style %-formatting to str.format().

  - Using named variable references in email templates instead of %%s.

* Fixes:

  - Fixed sending emails to multiple vsmlist-hosted mailing lists.

====================================================================================================
vsmlist 0.1.2 released (28.09.2013)
====================================================================================================

* Fixed a bug with emails having a Message-IDs containing a '/'. These caused the program to crash.

====================================================================================================
vsmlist 0.1.1 released (19.03.2013)
====================================================================================================

* Added support for multiple recipients in both To and Cc. Multiple mailing lists can be addressed
  with the same email now.

* Added storing the Message-IDs of already-processed emails, so that one email will only be
  processed once.

====================================================================================================
vsmlist 0.1 released (16.09.2012)
====================================================================================================

* Initial release
